# oauth-proxy

An [OmniAuth](https://github.com/intridea/omniauth)-like authentication service
as a reverse proxy. Sits in front of your application, and provides the current
user's authentication information in HTTP request headers, without exposing any
details to the end user.